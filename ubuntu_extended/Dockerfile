FROM ubuntu:16.04
MAINTAINER Lukasz Laszko <lukaszlaszko@docker.com>

RUN echo "deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-3.9 main" >> /etc/apt/sources.list
RUN echo "deb-src http://apt.llvm.org/xenial/ llvm-toolchain-xenial-3.9 main" >> /etc/apt/sources.list

# install tools and compilers
RUN apt-get -y update
RUN apt-get install -y xz-utils wget curl sudo
RUN apt-get install -y cmake ninja-build git-all doxygen
RUN apt-get install -y libboost-all-dev
RUN apt-get install -y --allow-unauthenticated clang-3.9 lldb-3.9
RUN apt-get install -y g++ gdb
RUN apt-get install -y python-pip
RUN apt-get install -y python3-pip
RUN apt-get install -y libxerces-c-dev
RUN apt-get install -y pkg-config libcapstone-dev

# enable ssh access
RUN apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:screencast' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
