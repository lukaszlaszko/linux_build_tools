#!/usr/bin/env bash

while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in
        # Enables verbose logging
        -v|--verbose)
        verbose=1
        ;;
        # Name of image to delete
        -i|--image-name)
        shift # past the key and to the value
        image_name="$1"
        ;;
        *)
        # Do whatever you want with extra options
        echo "Unknown option '$key'"
        ;;
    esac
    # Shift after checking all the cases to get the next option
    shift
done

if [[ ${verbose+x} ]]; then
    set -x
fi
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

echoerr() { printf "%s\n" "$*" >&2; }
echoverbose() { if [[ ${verbose+x} ]]; then printf "%s\n" "$*" >&2; fi }

# verify mandatory arguments
if [[ ! ${image_name+x} ]]; then
    echoerr "-i|image_name is mandatory!"
    exit 1 
fi

# locate docker
docker_cli=$(which docker)
if ! [[ ${docker_cli+x} ]]; then
    echoerr "docker cannot be located!"
    exit 1
fi

if [[ $(ps -a | grep dockerd | wc -l) -eq "0" ]]; then
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        $docker_cli -d
        if [ $status -ne 0 ]; then
            echoerr "Can't start docker daemon"
            exit 1
        fi
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        open -a Docker
        if [ $status -ne 0 ]; then
            echoerr "Can't start docker daemon"
            exit 1
        fi
    else
        echoerr "Unsupported platform $OSTYPE"
        exit 1
    fi
fi

# remove related containers & image
$docker_cli ps -a | awk '{ print $1,$2 }' | grep $image_name | awk '{print $1 }' | xargs -I {} docker rm {}
$docker_cli rmi $image_name