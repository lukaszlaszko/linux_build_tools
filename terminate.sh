#!/usr/bin/env bash

while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in
        # Enables verbose logging
        -v|--verbose)
        verbose=1
        ;;
        # Tag of the container with image to terminate
        -t|--tag)
        shift # past the key and to the value
        tag="$1"
        ;;
        *)
        # Do whatever you want with extra options
        echo "Unknown option '$key'"
        ;;
    esac
    # Shift after checking all the cases to get the next option
    shift
done

if [[ ${verbose+x} ]]; then
    set -x
fi
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

echoerr() { printf "%s\n" "$*" >&2; }
echoverbose() { if [[ ${verbose+x} ]]; then printf "%s\n" "$*" >&2; fi }

# verify mandatory arguments
if [[ ! ${tag+x} ]]; then
    echoerr "-tag|--tag is mandatory!"
    exit 1 
fi

docker_cli=$(which docker)
if ! [[ ${docker_cli+x} ]]; then
    echoerr "docker cannot be located!"
    exit 1
fi

# stop and remove container
repository_name=$(basename $(git remote show -n origin | grep Fetch | cut -d: -f2-))
image_name=${repository_name%.*}
container_name=${image_name}_${tag}__instance
$docker_cli stop $container_name > /dev/null
$docker_cli rm $container_name > /dev/null