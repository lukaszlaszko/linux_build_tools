#!/usr/bin/env bash

# defaults
ssh_port=20023

# parse arguments
while [[ $# -gt 0 ]]; do
    key="$1"
    case "$key" in
        # Enables verbose logging
        -v|--verbose)
        verbose=1
        ;;
        # Tag of the image to launch
        -t|--tag)
        shift # past the key and to the value
        tag="$1"
        ;;
        # Base mapped directory exposed as /opt/host
        -m|--mapped-base)
        shift # past the key and to the value
        mapped_dir="$1"
        ;;
        # Mapped port for SSH
        -p|--ssh-port)
        shift # past the key and to the value
        ssh_port="$1"
        ;;
        # Maps current directory if mapping is missing
        -c|--map-current)
        map_current=1
        ;;
        *)
        # Do whatever you want with extra options
        echo "Unknown option '$key'"
        ;;
    esac
    # Shift after checking all the cases to get the next option
    shift
done

if [[ ${verbose+x} ]]; then
    set -x
fi
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

echoerr() { printf "%s\n" "$*" >&2; }
echoverbose() { if [[ ${verbose+x} ]]; then printf "%s\n" "$*" >&2; fi }

# verify mandatory arguments
if [[ ! ${tag+x} ]]; then
    echoerr "tag hasn't been specified.'"
    exit 1
fi

if [[ ! ${mapped_dir+x} ]]; then
    if [[ ${map_current+x} ]]; then
        mapped_dir=$(realpath .)
        echo "mapped_dir hasn't been specified. Using current dir $mapped_dir"
    else
        echoerr "Either -m|--mapped-dir or -c|--map-current options is required!"
        exit 1
    fi
else
    mapped_dir=$(realpath $mapped_dir)
fi

docker_cli=$(which docker)
if ! [[ ${docker_cli+x} ]]; then
    echoerr "docker cannot be located!"
    exit 1
fi

# start container
repository_name=$(basename $(git remote show -n origin | grep Fetch | cut -d: -f2-))
image_name=${repository_name%.*}

search_result=$($docker_cli images "$image_name:$tag" | tail -n +2)
if [[ -z "$search_result" ]]; then
    search_result=$($docker_cli search $image_name | tail -n +2)
    result_count=$(echo $search_result | wc -l)
else
    result_count=1
fi

if [[ $result_count -eq 1 ]]; then
    full_image_name="$(echo $search_result | cut -d ' ' -f 1):$tag"
    echo "Starting: $full_image_name"
    container_name=${image_name}_${tag}__instance
    $docker_cli run -d -v $mapped_dir:/opt/host -p 127.0.0.1:$ssh_port:22 --security-opt seccomp=unconfined --name $container_name $full_image_name > /dev/null
else
    echoerr "$image_name registry contains $result_count matching $image_name!"
    exit 1
fi