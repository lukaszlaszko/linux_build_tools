[![Sources](https://img.shields.io/badge/bitbucket-sources-green.svg?style=flat)](https://bitbucket.org/lukaszlaszko/linux_build_tools/src)
## Intention

This repository intends to track Dockerfile(s) used to construct images published to [Dockerhub](https://hub.docker.com) under [lukaszlaszko/linux_build_tools](https://hub.docker.com/r/lukaszlaszko/linux_build_tools/).

Details on how to use dockerfiles can be found here:
https://docs.docker.com/engine/getstarted/step_four/

## Structure

Each of sub-directories in this repository should contain a definition of an image published under its name tag to [lukaszlaszko/linux_build_tools](https://hub.docker.com/r/lukaszlaszko/linux_build_tools/). 

Components included:

* **ubuntu_extended**
	- Ubuntu 16.10
	- clang 3.9 and lldb-3.9
	- gcc 6.2 and gdb
	- cmake
	- ninja
	- python 2 & python 3
	- boost 1.61
	- ssh server
	- root:screencast

	Launch command:

		$ ./launch.sh -t ubuntu_extended -m .. -p 20023

	Terminate command:

		$ ./terminate.sh -t ubuntu_extended

	[Docker file](https://bitbucket.org/lukaszlaszko/linux_build_tools/raw/HEAD/ubuntu_extended/Dockerfile)

* **centos_basic**
	- CentOS 7.3.1611
	- Development tools

	This image is intended to be used as a fundation to other images in the repository. As such it does not constitue ssh endpoint 
	and should not be launched with `launch.sh` script.

	[Docker file](https://bitbucket.org/lukaszlaszko/linux_build_tools/raw/HEAD/centos_basic/Dockerfile)

## Repository

Docker files defining images and utility scripts are stored in a bitbucket repository and can be cloned with:

	$ git clone https://bitbucket.org/lukaszlaszko/linux_build_tools.git

## Lunching and terminating

Scripts in repo's root directory help to launch and terminate containers. As such:

* **launch.sh** may be used to start a container from image in [lukaszlaszko/linux_build_tools] marked with a given tag.

	$ ./launch.sh -t ubuntu_extended -m ..

* **terminate.sh** may be used to terminate a container running an image with given tag. This command will also delete all
local storage of the container.

	$ ./terminate.sh -t ubuntu_extended

## Building and testing

There are two scripts used to test new/modified configurations:

* **build.sh** builds an image locally. After build the image can be launched with `launch.sh`. 

* **delete** deletes an image with the given name from the local store. This alos removes all dependent containers. 

## How to publish

It's desired for images to be constructed using automated build system at docker hub. However, if manual publication 
is required: 

1. Create or edit a Dockerfile. 

    Each docker image should come in a directory which will indicate its tag a for image **linux_build_tools**
	
		$ mkdir centos_basic 
		
2. Build docker image with `build.sh`

		$ ./build.sh -t centos_basic
		
3. Retag the image to your desire

		$ docker tag linux_build_tools:centos_basic lukaszlaszko/linux_build_tools:centos_basic_manual
		
	To list local images with tags:
	
		$ docker images

4. Push the tagged image into [Dockerhub](https://hub.docker.com).

		$ docker login
		Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
		Username (lukaszlaszko): 
		Password: 
		Login Succeeded
		$ docker push lukaszlaszko/linux_build_tools:centos_basic_manual
		The push refers to a repository [docker.io/lukaszlaszko/linux_build_tools:centos_basic_manual]
		5914a2c48ee6: Pushed 
		33eda3dd8fcf: Pushed 
		c1bd37d01c89: Layer already exists 
		943edb549a83: Layer already exists 
		bf6751561805: Layer already exists 
		f934e33a54a6: Layer already exists 
		e7ebc6e16708: Layer already exists 
		latest: digest: sha256:426c6a3e9beec233aa28893d365f90a01b6581aac7ba3b023a91bc221467a243 size: 1783